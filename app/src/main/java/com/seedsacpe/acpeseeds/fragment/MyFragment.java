package com.seedsacpe.acpeseeds.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.seedsacpe.acpeseeds.Net.TaskType;
import com.seedsacpe.acpeseeds.R;
import com.telpoo.frame.ui.BetaBaseFragment;

public class MyFragment extends BetaBaseFragment implements TaskType {
    protected View v;
    int page = 1;
    int limit = 30;
    Boolean cancelRequest = true;
    protected View progressBar, tvNodata;

    public MyFragment() {

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("fragment", "onStart");
    }


    @Override
    public void onCreate(Bundle arg0) {
        Log.d("fragment", getClass().getSimpleName());
        super.onCreate(arg0);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (v == null) return;
//        tvNodata = v.findViewById(R.id.viewNodata);
        progressBar = v.findViewById(R.id.progressBar);
    }

    @Override
    public void onFail(int taskType, String msg) {
        super.onFail(taskType, msg);
        hindeProgress();

    }

    @Override
    public void onSuccess(int taskType, Object data, String msg) {
        super.onSuccess(taskType, data, msg);
        hindeProgress();
    }

    public void hindeProgress() {
        if (progressBar == null) return;
        progressBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
            }
        }, 1000);

    }

    public void showProgress() {
        if (progressBar == null) return;
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hindeNoData() {
        if (tvNodata == null) return;
        tvNodata.setVisibility(View.GONE);
    }

    public void showNoData() {
        if (tvNodata == null) return;
        tvNodata.setVisibility(View.VISIBLE);
    }

    public void sendBroadcast(String keyBroadcast) {
        try {
            getActivity().sendBroadcast(new Intent(keyBroadcast));
        } catch (Exception e) {

        }
    }

    public void showToast(String message) {
        if (message != null && getActivity() != null)
            Toast.makeText(getActivity(), "" + message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void closeProgressDialog() {
        if (getActivity() == null || getActivity().isFinishing()) return;
        super.closeProgressDialog();
    }

}
