package com.seedsacpe.acpeseeds.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.seedsacpe.acpeseeds.MyApplication;
import com.seedsacpe.acpeseeds.Net.TaskNet;
import com.seedsacpe.acpeseeds.R;
import com.seedsacpe.acpeseeds.SubCategoryActivity;
import com.seedsacpe.acpeseeds.adapter.ItemsAdapterHolderAds;
import com.telpoo.frame.model.BaseModel;
import com.telpoo.frame.object.BaseObject;

import java.util.ArrayList;

public class SubCategoryFmAds extends MyFragment {
    BaseObject item;
    ItemsAdapterHolderAds adapter;
    RecyclerView mRecyclerView;
    RelativeLayout rlViewDialogAds;

    public SubCategoryFmAds() {

    }

    @SuppressLint("ValidFragment")
    public SubCategoryFmAds(BaseObject item) {
        this.item = item;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        if (v != null) return v;
        v = inflater.inflate(R.layout.fragment_list, container, false);
        rlViewDialogAds = v.findViewById(R.id.rlViewDialogAds);
        adapter = new ItemsAdapterHolderAds(getActivity(), item.get("name", ""), rlViewDialogAds);
        mRecyclerView = v.findViewById(R.id.list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int pastVisibleItems, visibleItemCount, totalItemCount;
                visibleItemCount = mRecyclerView.getLayoutManager().getChildCount();
                totalItemCount = mRecyclerView.getLayoutManager().getItemCount();
                pastVisibleItems = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                Log.d("mRecyclerView", "visibleItemCount: " + visibleItemCount + " | pastVisibleItems: " + pastVisibleItems + " | totalItemCount: " + totalItemCount);
                if ((visibleItemCount + pastVisibleItems) >= totalItemCount && totalItemCount >= limit) {
                    loadData();
                }
            }
        });
        mRecyclerView.setAdapter(adapter);
        loadData();
        MyApplication.getInstance().trackEvent("Screen", ((SubCategoryActivity) getActivity()).getCategoryName(), item.get("name", ""));
        return v;
    }

    public void loadData() {
        if (!cancelRequest) return;
        cancelRequest = false;

        TaskNet net = new TaskNet(new BaseModel() {
            @Override
            public void onSuccess(int taskType, Object data, String msg) {
                super.onSuccess(taskType, data, msg);
                hindeProgress();
                ArrayList<BaseObject> list = (ArrayList<BaseObject>) data;
                if (page == 0) adapter.setData(list);
                else adapter.addData(list);
                if (adapter.getItemCount() == 0) showNoData();
                else hindeNoData();
                if (list.size() == limit) {
                    page += 1;
                    cancelRequest = true;
                }
            }

            @Override
            public void onFail(int taskType, String msg) {
                super.onFail(taskType, msg);
                Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                hindeProgress();
            }
        }, TASK_ITEMS, getActivity());

        BaseObject object = new BaseObject();
        object.set("cat", item.get("cat_id", ""));
        object.set("per_page", limit);
        object.set("page", page);

        net.setTaskParram("parram", object);
        net.exe();
        showProgress();
    }

}
