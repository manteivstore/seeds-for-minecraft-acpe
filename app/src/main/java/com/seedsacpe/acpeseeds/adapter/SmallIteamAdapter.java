package com.seedsacpe.acpeseeds.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdSize;
import com.seedsacpe.acpeseeds.ItemDetailActivity;
import com.seedsacpe.acpeseeds.MyApplication;
import com.seedsacpe.acpeseeds.Net.SPRsupport;
import com.seedsacpe.acpeseeds.R;
import com.seedsacpe.acpeseeds.ads.AdsLoader;
import com.seedsacpe.acpeseeds.ads.listener;
import com.seedsacpe.acpeseeds.bean.CategoryObj;
import com.seedsacpe.acpeseeds.bean.ItemObj;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.telpoo.frame.object.BaseObject;

import java.util.ArrayList;
import java.util.Collections;

public class SmallIteamAdapter extends RecyclerView.Adapter<SmallIteamAdapter.ViewHolder> {
    private ArrayList<BaseObject> smallList = new ArrayList<>();
    Activity activity;
    LayoutInflater mInflay;
    boolean isCheck;
    int post_like = 0;
    RelativeLayout rlViewDialogAds;

    public SmallIteamAdapter(Activity activity, RelativeLayout rlViewDialogAds) {
        this.activity = activity;
        this.rlViewDialogAds = rlViewDialogAds;
        mInflay = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<BaseObject> tmps) {
        smallList.clear();
        smallList.addAll(tmps);
        Collections.shuffle(smallList);
        notifyDataSetChanged();
    }

    public void addData(ArrayList<BaseObject> tmps) {
        smallList.clear();
        smallList.addAll(tmps);
        Collections.shuffle(smallList);
        notifyDataSetChanged();
    }

    public BaseObject getItem(int i) {
        return smallList.get(i);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View sView = mInflay.inflate(R.layout.item, viewGroup, false);
        return new ViewHolder(sView);
    }

    int check = -1;

    // TODO: 8/2/20 ThaoTm rên list này hiện ở 3 vị trí
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position > check && holder.rlViewBanner.getChildCount() == 0) {
            switch (position) {
                case 1:

                case 4:
                    AdsLoader.getInstall(activity).showBanner(holder.rlViewBanner, AdSize.MEDIUM_RECTANGLE, 2);
                    break;
            }
        }
        check++;

        BaseObject item = getItem(position);
        holder.tvName.setText(item.get(ItemObj.name, ""));
        holder.tvLike.setText(item.get(ItemObj.post_like, ""));
        holder.tvDownload.setText(item.get(ItemObj.download, ""));
        holder.tvView.setText(item.get(ItemObj.views, ""));

        isCheck = SPRsupport.isLike(item.get(ItemObj.id, ""), activity);
        post_like = Integer.parseInt(item.get(ItemObj.post_like, ""));
        if (isCheck) {
            post_like = post_like + 1;
            holder.tvLike.setText(post_like + "");
        }

        ImageLoader.getInstance().displayImage(item.get(ItemObj.thumbnail, ""),
                holder.imgAvatar, MyApplication.getImageOption(), new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        holder.layoutShimmer.startShimmer();
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        holder.layoutShimmer.stopShimmer();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        holder.layoutShimmer.stopShimmer();
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        holder.layoutShimmer.stopShimmer();
                    }
                });
    }

    @Override
    public int getItemCount() {
        if (smallList.size() > 10) return 10;
        else return smallList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgAvatar;
        TextView tvName;
        TextView tvLike;
        TextView tvComment;
        TextView tvDownload;
        TextView tvView;
        ShimmerFrameLayout layoutShimmer;
        private RelativeLayout rlViewBanner;

        public ViewHolder(View itemView) {
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.img);
            tvName = itemView.findViewById(R.id.tvName);
            tvLike = itemView.findViewById(R.id.tvLike);
            tvComment = itemView.findViewById(R.id.tvComment);
            tvDownload = itemView.findViewById(R.id.tvDownload);
            tvView = itemView.findViewById(R.id.tvView);
            layoutShimmer = itemView.findViewById(R.id.layoutShimmer);
            rlViewBanner = itemView.findViewById(R.id.rlViewBanner);
            rlViewBanner.setVisibility(View.GONE);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseObject item = getItem(getAdapterPosition());
                    ImageLoader.getInstance().loadImage(item.get(CategoryObj.image, ""), null);
                    Intent intent = new Intent(activity, ItemDetailActivity.class);
                    intent.putExtra("data", item);
                    intent.putExtra("type", "smalllisst");
                    intent.putExtra("category", (item.get(CategoryObj.name, "")));
                    AdsLoader.getInstall(activity).showFullAdmob(new listener.adsLoadShowFull() {
                        @Override
                        public void onLoadFailAds() {
                            activity.startActivityForResult(intent, 1);
                            activity.finish();
                        }

                        @Override
                        public void onAdClosed() {
                            activity.startActivityForResult(intent, 1);
                            activity.finish();
                        }
                    });
                }
            });
        }
    }
}
