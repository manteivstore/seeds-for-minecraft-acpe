package com.seedsacpe.acpeseeds.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.seedsacpe.acpeseeds.R;
import com.seedsacpe.acpeseeds.bean.CommentObj;
import com.telpoo.frame.object.BaseObject;

import java.util.ArrayList;

public class ReplyCmtAdapter extends RecyclerView.Adapter<ReplyCmtAdapter.ViewHolder> {

    private ArrayList<BaseObject> mListReplyCmt = new ArrayList<>();
    Activity activity;
    LayoutInflater mInflay;


    public ReplyCmtAdapter(Activity activity) {
        this.activity = activity;
        mInflay = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<BaseObject> tmps) {
        mListReplyCmt.clear();
        for (int i = 0; i < tmps.size(); i++) {
            BaseObject item = tmps.get(i);
            mListReplyCmt.add(item);
        }
        notifyDataSetChanged();
    }

    public void addData(ArrayList<BaseObject> tmps) {
        mListReplyCmt.addAll(tmps);
        notifyDataSetChanged();
    }

    public BaseObject getItem(int i) {
        return mListReplyCmt.get(i);
    }


    public ArrayList<BaseObject> getAll() {
        return mListReplyCmt;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cmt_reply, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BaseObject itemCmt = getItem(position);
        holder.tvContentReply.setText(itemCmt.get(CommentObj.comment, ""));
        holder.tvTimeReply.setText(itemCmt.get(CommentObj.comment_date, ""));

    }

    @Override
    public int getItemCount() {
        return mListReplyCmt.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvContentReply;
        TextView tvTimeReply;

        public ViewHolder(View itemView) {
            super(itemView);
            tvContentReply = itemView.findViewById(R.id.tvContentReply);
            tvTimeReply = itemView.findViewById(R.id.tvTimeReply);
        }
    }
}
