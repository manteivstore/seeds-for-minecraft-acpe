package com.seedsacpe.acpeseeds.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.seedsacpe.acpeseeds.ItemDetailActivity;
import com.seedsacpe.acpeseeds.Net.SPRsupport;
import com.seedsacpe.acpeseeds.R;
import com.seedsacpe.acpeseeds.SubCategoryActivity;
import com.seedsacpe.acpeseeds.ads.AdsLoader;
import com.seedsacpe.acpeseeds.ads.listener;
import com.seedsacpe.acpeseeds.bean.ItemObj;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.seedsacpe.acpeseeds.MyApplication;
import com.seedsacpe.acpeseeds.bean.CategoryObj;
import com.telpoo.frame.object.BaseObject;

import java.util.ArrayList;

public class ItemsAdapterHolder extends RecyclerView.Adapter<ItemsAdapterHolder.ViewHolder> {
    private ArrayList<BaseObject> mList = new ArrayList<>();
    Activity activity;
    LayoutInflater mInflay;
    String type = "";
    boolean isCheck;
    int post_like = 0;
    RelativeLayout rlViewDialogAds;

    public ItemsAdapterHolder(Activity activity, String type, RelativeLayout rlViewDialogAds) {
        this.type = type;
        this.activity = activity;
        this.rlViewDialogAds = rlViewDialogAds;
        mInflay = LayoutInflater.from(activity);
    }

    public ItemsAdapterHolder(Activity activity) {
        this.activity = activity;
        mInflay = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<BaseObject> tmps) {
        mList.clear();
        mList.addAll(tmps);
        notifyDataSetChanged();
    }

    public void addData(ArrayList<BaseObject> tmps) {
        mList.clear();
        mList.addAll(tmps);
        notifyDataSetChanged();
    }

    public BaseObject getItem(int i) {
        return mList.get(i);
    }

    public ArrayList<BaseObject> getAll() {
        return mList;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @SuppressWarnings("ResourceType")
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View sView = mInflay.inflate(R.layout.item, parent, false);
        return new ViewHolder(sView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        BaseObject item = getItem(position);
        holder.tvLike.setText(item.get(ItemObj.post_like, ""));
        holder.tvDownload.setText(item.get(ItemObj.download, ""));
        holder.tvView.setText(item.get(ItemObj.views, ""));
        isCheck = SPRsupport.isLike(item.get(ItemObj.id, ""), activity);
        post_like = Integer.parseInt(item.get(ItemObj.post_like, ""));
        if (isCheck) {
            post_like = post_like + 1;
            holder.tvLike.setText(post_like + "");
        }
        holder.tvName.setText(item.get(CategoryObj.name, ""));
        ImageLoader.getInstance().displayImage(item.get(CategoryObj.thumbnail, ""), holder.imgAvatar, MyApplication.getImageOption(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.layoutShimmer.startShimmer();
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.layoutShimmer.stopShimmer();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.layoutShimmer.stopShimmer();
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                holder.layoutShimmer.stopShimmer();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgAvatar;
        TextView tvName;
        TextView tvLike;
        TextView tvComment;
        TextView tvDownload;
        TextView tvView;
        ShimmerFrameLayout layoutShimmer;

        public ViewHolder(View row) {
            super(row);
            imgAvatar = row.findViewById(R.id.img);
            tvName = row.findViewById(R.id.tvName);
            tvLike = row.findViewById(R.id.tvLike);
            tvComment = row.findViewById(R.id.tvComment);
            tvDownload = row.findViewById(R.id.tvDownload);
            tvView = row.findViewById(R.id.tvView);
            layoutShimmer = row.findViewById(R.id.layoutShimmer);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseObject item = getItem(getAdapterPosition());
                    ImageLoader.getInstance().loadImage(item.get(CategoryObj.image, ""), null);
                    Intent intent = new Intent(activity, ItemDetailActivity.class);
                    intent.putExtra("data", item);
                    intent.putExtra("type", type);
                    intent.putExtra("category", ((SubCategoryActivity) activity).getCategoryName());
                    AdsLoader.getInstall(activity).showFullAdmob(new listener.adsLoadShowFull() {
                        @Override
                        public void onLoadFailAds() {
                            activity.startActivityForResult(intent, 1);
                        }

                        @Override
                        public void onAdClosed() {
                            activity.startActivityForResult(intent, 1);
                        }
                    });
                }
            });
        }
    }
}
