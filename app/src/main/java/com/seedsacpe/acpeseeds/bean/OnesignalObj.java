package com.seedsacpe.acpeseeds.bean;

public class OnesignalObj {
    public static String uid = "uid";
    public static String country = "country";
    public static String app_name = "app_name";
    public static String package_name = "package_name";
    public static String version = "version";
    public static String flavor = "flavor";
    public static String link = "link";
    public static String time = "time";
    public static String status = "status";
    public static String type = "type";
    public static String msg_id = "msg_id";
}
