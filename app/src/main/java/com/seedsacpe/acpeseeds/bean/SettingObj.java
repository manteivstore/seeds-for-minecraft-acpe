package com.seedsacpe.acpeseeds.bean;

public class SettingObj {
    public static String enable = "enable";
    public static String close = "close";
    public static String time = "time";

    public static String enable_screen = "enable_screen";
    public static String enable_poup = "enable_poup";

    public static String preview = "preview";
    public static String app_id = "app_id";
    public static String type = "type";
    public static String link = "link";

    public static final String TYPE_APK = "apk";
    public static final String TYPE_LINK = "link";
    public static final String TYPE_STORE = "playstore";
}
