package com.seedsacpe.acpeseeds.updatecontent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class USBEventReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        //Thằng này nó chạy mặc định trong app, trừ khi gỡ app.
        UpdateWorker.start(context);
        Log.d("SonLv", "USBEventReceiver");
    }

}
