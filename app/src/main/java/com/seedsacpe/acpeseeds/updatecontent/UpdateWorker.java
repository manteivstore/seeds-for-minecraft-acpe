package com.seedsacpe.acpeseeds.updatecontent;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.seedsacpe.acpeseeds.ads.AdsLoader;
import com.seedsacpe.acpeseeds.ads.AdsSuppost;

import java.util.Calendar;

public class UpdateWorker extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("SonLv", "UpdateWorker: startActivity");
        AdsLoader.getInstall(context).initAds();

        GetDataService.startService(context);
    }

    public static void start(Context context) {
        Log.d("SonLv", "UpdateSmsWorker: start");
        GetDataService.startService(context);
        if (!SettingSuppost.enableTimeAlarm(context)) {
            Log.d("SonLv", "chưa đến giờ AlarmManager");
            return;
        }
        SettingSuppost.saveTimeAlarm(context);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, UpdateWorker.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 6, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        long time = 1000 * 60 * getTimeCheck(context);
        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() + time, time, alarmIntent);
    }

    public static int getTimeCheck(Context context) {
        return AdsSuppost.getInstall(context).getDataAds().optInt("time_check", 1);
    }

}
