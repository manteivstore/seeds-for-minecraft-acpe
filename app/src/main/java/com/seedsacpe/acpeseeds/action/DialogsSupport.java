package com.seedsacpe.acpeseeds.action;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;

import com.seedsacpe.acpeseeds.R;


public class DialogsSupport {
    public static void showDialogYesNo(Activity context, String btnYes, String btnNo, String title, String message, final Listener.OnDialogYesNoListener listener) {
        if (context.isFinishing()) return;
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            if (title != null) builder.setTitle(title);
            if (message != null) builder.setMessage(message);
            builder.setNegativeButton(btnYes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    listener.onYesClick();
                }
            });
            builder.setPositiveButton(btnNo, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    listener.onNoClick();
                }
            });
            builder.show();
        } catch (Exception e) {
            Log.d("DialogsSupport", "Exception: " + e.getMessage());
        }


    }


    public static void showDialogYes(Activity context, String btnYes, String title, String message, final Listener.OnDialogYesNoListener listener) {
        if (context.isFinishing()) return;
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setCancelable(false);
            if (message != null) builder.setMessage(message);
            builder.setNegativeButton(btnYes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    listener.onYesClick();
                }
            });
            builder.show();
        } catch (Exception e) {
            Log.d("DialogsSupport", "Exception: " + e.getMessage());
        }
    }


    public static void showDialogIAP(Activity activity, final Listener.OnDialogYesNoListener listener) {
        if (activity.isFinishing()) return;
        try {
            View view = LayoutInflater.from(activity).inflate(R.layout.dialog_iap, null);
            View btnPolicy = view.findViewById(R.id.btnPolicy);
            View btnTermOfUser = view.findViewById(R.id.btnTermOfUser);
            View btnPurchases = view.findViewById(R.id.btnPurchases);
            View btnFree = view.findViewById(R.id.btnFree);

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setView(view);
            AlertDialog alertDialog = builder.create();

            btnPurchases.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onYesClick();
                    alertDialog.dismiss();
                }
            });
            btnFree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onYesClick();
                    alertDialog.dismiss();
                }
            });

            btnPolicy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://modsforminecraft.info/policy/"));
                        activity.startActivity(myIntent);
                    } catch (ActivityNotFoundException e) {
                    }
                }
            });

            btnTermOfUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://farahenelysian.xyz/policy/terms-of-use.pdf"));
                        activity.startActivity(myIntent);
                    } catch (ActivityNotFoundException e) {
                    }

                }
            });
            alertDialog.show();

        } catch (Exception e) {
            Log.d("DialogsSupport", "Exception: " + e.getMessage());
        }
    }
}
