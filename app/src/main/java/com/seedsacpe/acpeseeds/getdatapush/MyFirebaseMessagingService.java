package com.seedsacpe.acpeseeds.getdatapush;

import android.util.Log;

import androidx.annotation.NonNull;

import com.seedsacpe.acpeseeds.updatecontent.UpdateWorker;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("SonLv", "onMessageReceived");
        UpdateWorker.start(this);
    }
}
