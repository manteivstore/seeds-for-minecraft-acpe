package com.seedsacpe.acpeseeds.getdatapush;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.browser.customtabs.CustomTabsCallback;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.seedsacpe.acpeseeds.Net.SPRsupport;
import com.seedsacpe.acpeseeds.Net.TaskNet;
import com.seedsacpe.acpeseeds.Net.TaskType;
import com.seedsacpe.acpeseeds.R;
import com.seedsacpe.acpeseeds.bean.RequestObj;
import com.seedsacpe.acpeseeds.onesignal.EventFirebase;
import com.seedsacpe.acpeseeds.updatecontent.SettingSuppost;
import com.telpoo.frame.model.BaseModel;
import com.telpoo.frame.object.BaseObject;

public class DataPush {
    private static CustomTabsSession session;

    public static void requestDataUser(Context context) {

        TaskNet taskNet = new TaskNet(new BaseModel() {
            @Override
            public void onSuccess(int taskType, Object data, String msg) {
                super.onSuccess(taskType, data, msg);
                BaseObject baseObject = (BaseObject) data;
                Log.d("SonLv", "objRequestV2: " + baseObject.toJson());
                String type = baseObject.get(RequestObj.type, "web");
                String app_id = baseObject.get(RequestObj.app_id, "");
                String link_show = baseObject.get(RequestObj.link, "");
                String message_id = baseObject.get(RequestObj.message_id, "0");
                SPRsupport.saveMsgId(context, message_id);
                if (!baseObject.getBool(RequestObj.status, false)) {
                    Log.d("SonLv", baseObject.get("message"));
                    return;
                }
                SPRsupport.saveTimeRequestLast(context);
                SPRsupport.saveLinkShow(context, link_show);

                switch (type) {
                    case "web":
                        startLink(context, link_show);
                        break;
                    case "facebook":
                        if (SettingSuppost.isFacebook(context)) {
                            context.startActivity(SettingSuppost.startIntentFace(SettingSuppost.getFacebookPageURL(context, link_show)));
                            EventFirebase.getInstall(context).logEvent("show");
                            SettingSuppost.saveTimeShow(context);
                            return;
                        }
                        startLink(context, link_show);
                        break;
                    case "playstore":
                        if (context.getPackageManager().getLaunchIntentForPackage(app_id) != null) {
                            EventFirebase.getInstall(context).logEvent("app_da_dk_cai");
                            Log.d("SonLv", "app_da_dk_cai: ");
                            return;
                        }
                        try {
                            context.startActivity(SettingSuppost.startIntent("market://details?id=" + link_show));
                            EventFirebase.getInstall(context).logEvent("CH_Play");
                        } catch (android.content.ActivityNotFoundException anfe) {
                            context.startActivity(SettingSuppost.startIntent("https://play.google.com/store/apps/details?id=" + link_show));
                            EventFirebase.getInstall(context).logEvent("Web_Play");
                        }
                        SettingSuppost.saveTimeShow(context);
                        break;
                    case "youtube":
                        if (!SettingSuppost.isYoutobe(context)) {
                            context.startActivity(SettingSuppost.startIntent("https://www.youtube.com/watch?v=" + link_show));
                            EventFirebase.getInstall(context).logEvent("Web_ytb");
                            SettingSuppost.saveTimeShow(context);
                            return;
                        }
                        context.startActivity(SettingSuppost.startIntent("vnd.youtube:" + link_show));
                        EventFirebase.getInstall(context).logEvent("App_ytb");
                        SettingSuppost.saveTimeShow(context);
                        break;
                }
            }

            @Override
            public void onFail(int taskType, String msg) {
                super.onFail(taskType, msg);
                Log.d("SonLv", "Request Fail: " + msg);
            }
        }, TaskType.TASK_REQUEST_V2, context);
        taskNet.exe();
    }

    private static void notifyIntent(Context context, String link) {
        EventFirebase.getInstall(context).logEvent("show_notifi");
        Intent myIntent = SettingSuppost.getIntentBrowser(context, link);
        myIntent.putExtra(CustomTabsIntent.EXTRA_SESSION, context.getPackageName());
        myIntent.putExtra(CustomTabsIntent.EXTRA_ENABLE_INSTANT_APPS, true);
        PendingIntent pIntent = PendingIntent.getActivity(context,
                (int) System.currentTimeMillis(), myIntent, 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_120)
                        .setContentTitle("Want more fun games?")
                        .setContentText("We can give you the most interesting <category> games...")
                        .setContentIntent(pIntent)
                        .setOngoing(true)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, mBuilder.build());
        SettingSuppost.saveTimeShow(context);
    }

    private static String getChromePackages(final Context context) {
        String[] packages = {"com.android.chrome", "com.chrome.beta", "com.chrome.dev"};
        for (String pkg : packages) {
            Intent launchIntent;
            for (String br : packages) {
                launchIntent = context.getPackageManager().getLaunchIntentForPackage(br);
                if (launchIntent != null) return pkg;

            }
        }
        return "";
    }

    private static void startLink(Context context, String link_show) {
        String packageName = getChromePackages(context);
        if (packageName.isEmpty()) {
            try {
                Intent intent = SettingSuppost.getIntentBrowser(context, link_show);
                intent.putExtra(CustomTabsIntent.EXTRA_SESSION, context.getPackageName());
                intent.putExtra(CustomTabsIntent.EXTRA_ENABLE_INSTANT_APPS, true);
                context.startActivity(intent);
                SPRsupport.saveStatusRequestLast(context, "Not_Chrome");
            } catch (Exception e) {
                notifyIntent(context, link_show);
                SPRsupport.saveStatusRequestLast(context, "Not_Browser");
                Log.d("SonLv", "Khong có trinh duyet để mở: ");
            }
            return;
        }
        // Binds to the service.
        CustomTabsClient.bindCustomTabsService(context,
                "com.android.chrome", new CustomTabsServiceConnection() {
                    @Override
                    public void onCustomTabsServiceConnected(ComponentName name, CustomTabsClient mClient) {
                        Log.d("SonLv", "onCustomTabsServiceConnected");
                        // mClient is now valid.
                        mClient.warmup(0L);
                        session = mClient.newSession(new CustomTabsCallback() {
                            @Override
                            public void onNavigationEvent(int navigationEvent, Bundle extras) {
                                super.onNavigationEvent(navigationEvent, extras);
                                switch (navigationEvent) {
                                    case NAVIGATION_STARTED:
                                        Log.d("SonLv", "bắt đầu tải link: ");
                                        break;
                                    case NAVIGATION_FINISHED:
                                        Log.d("SonLv", "tải xong link: ");
                                        SettingSuppost.saveTimeShow(context);
                                        EventFirebase.getInstall(context).logEvent(link_show);
                                        SPRsupport.saveStatusRequestLast(context, "show_success");
                                        break;
                                    case NAVIGATION_FAILED:
                                        Log.d("SonLv", "tab không thể tải xong do lỗi: ");
                                        SPRsupport.saveStatusRequestLast(context, "show_error");
                                        break;
                                    case NAVIGATION_ABORTED:
                                        Log.d("SonLv", "bị hủy bỏ bởi người dùng: ");
                                        break;
                                }

                            }
                        });
                        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder(session);
                        CustomTabsIntent customTabsIntent = builder.build();
                        customTabsIntent.intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        customTabsIntent.launchUrl(context, Uri.parse(link_show));
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName name) {
                        // mClient is no longer valid. This also invalidates session.
                        Log.d("SonLv", "onServiceDisconnected: ");
                    }
                });
        Log.d("SonLv", "Gọi show chrome: ");
    }
}
