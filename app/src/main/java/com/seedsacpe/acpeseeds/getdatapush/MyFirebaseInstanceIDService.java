package com.seedsacpe.acpeseeds.getdatapush;

import android.util.Log;

import com.seedsacpe.acpeseeds.Net.SPRsupport;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("SonLv", "Refreshed token: " + refreshedToken);
        SPRsupport.saveToken(getApplicationContext(), refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }
}