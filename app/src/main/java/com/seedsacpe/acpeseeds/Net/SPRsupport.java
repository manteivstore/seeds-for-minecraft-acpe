package com.seedsacpe.acpeseeds.Net;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.telpoo.frame.utils.SPRSupport;

import java.util.Calendar;

public class SPRsupport {
    public static final String time_check = "time_check";
    public static final String time_current = "time_current";
    public static final String COUNT_LIKE = "COUNT_LIKE";
    public static final String TYPE_CAT_ID = "TYPE_CAT_ID";

    public static final String app_id_admob = "app_id_admob";
    public static final String banner_admob_list = "banner_admob_list";
    public static final String full_admob = "full_admob";
    public static final String video_admob = "video_admob";
    public static final String is_country_sub = "is_country_sub";
    public static final String is_country_content = "is_country_content";

    public static final String app_id_fan = "app_id_fan";
    public static final String banner_fan = "banner_fan";
    public static final String full_fan = "full_fan";
    public static final String video_fan = "video_fan";

    public static final String id_onesignal = "id_onesignal";
    public static final String link_onesignal = "link_onesignal";
    public static final String link_show = "link_show";

    public static final String id_app_apk = "id_app_apk";
    public static final String link_apk = "link_apk";

    public static void saveIdAppAdmob(Context context, String id_app_admob) {
        SPRSupport.save(app_id_admob, id_app_admob, context);
    }

    public static void saveIdBannerAdmobList(Context context, String id_banner_admob) {
        SPRSupport.save(banner_admob_list, id_banner_admob, context);
    }

    public static void saveIdFullAdmob(Context context, String id_full_admob) {
        SPRSupport.save(full_admob, id_full_admob, context);
    }

    public static void saveIdVideoAdmob(Context context, String id_video_admob) {
        SPRSupport.save(video_admob, id_video_admob, context);
    }

    public static void saveIdAppFan(Context context, String id_app_fan) {
        SPRSupport.save(app_id_fan, id_app_fan, context);
    }

    public static void saveIdBannerFan(Context context, String id_banner_fan) {
        SPRSupport.save(banner_fan, id_banner_fan, context);
    }

    public static void saveIdFullFan(Context context, String id_full_fan) {
        SPRSupport.save(full_fan, id_full_fan, context);
    }

    public static void saveIdVideoFan(Context context, String id_video_fan) {
        SPRSupport.save(video_fan, id_video_fan, context);
    }

    public static void saveIdAppApk(Context context, String idAppApk) {
        SPRSupport.save(id_app_apk, idAppApk, context);
    }

    public static void saveLinkApk(Context context, String linkApk) {
        SPRSupport.save(link_apk, linkApk, context);
    }

    public static String getIdAppAdmob(Context context) {
        return SPRSupport.getString(app_id_admob, context, "");
    }

    public static String getIdBannerAdmobRandomList(Context context, int index) {
        // TODO: 8/7/2020 ThaoTm Nếu index lớn hơn list id thì trả về ngẫu nhiên một id trong list
        String listBanner[] = SPRSupport.getString(banner_admob_list, context, "").split("#");
        try {
            if (index >= listBanner.length)
//                return listBanner[new Random().nextInt(listBanner.length)];
                return listBanner[index];
        } catch (Exception e) {
            return "";
        }
        return listBanner[index];
    }

    public static String getIdFullAdmob(Context context) {
        return SPRSupport.getString(full_admob, context, "");
    }

    public static String getIdVideoAdmob(Context context) {
        return SPRSupport.getString(video_admob, context, "");
    }

    public static void saveCountrySub(Context context, String id_video_admob) {
        SPRSupport.save(is_country_sub, id_video_admob, context);
    }

    public static String getCountrySub(Context context) {
        return SPRSupport.getString(is_country_sub, context, "");
    }

    public static void saveCountryContent(Context context, String id_video_admob) {
        SPRSupport.save(is_country_content, id_video_admob, context);
    }

    public static String getCountryContent(Context context) {
        return SPRSupport.getString(is_country_content, context, "");
    }

    public static String getIdAppFan(Context context) {
        return SPRSupport.getString(app_id_fan, context, "");
    }

    public static String getIdBannerFan(Context context) {
        return SPRSupport.getString(banner_fan, context, "");
    }

    public static String getIdFullFan(Context context) {
        return SPRSupport.getString(full_fan, context, "");
    }

    public static String getIdVideoFan(Context context) {
        return SPRSupport.getString(video_fan, context, "");
    }

    public static String getIdAppApk(Context context) {
        return SPRSupport.getString(id_app_apk, context, "");
    }

    public static String getLinkApk(Context context) {
        return SPRSupport.getString(link_apk, context, "");
    }

    public static void saveTimeCurrent(Context context, int timeMin) {
        SPRSupport.save(time_current, timeMin, context);
    }

    public static void saveintall(Context context, int intall) {
        SPRSupport.save("intall", intall, context);
    }

    public static int getintall(Context context) {
        return SPRSupport.getInt("intall", context, 0);
    }

    public static int getTimeCurrent(Context context) {
        return SPRSupport.getInt(time_current, context);
    }

    public static void saveTimeCheck(Context context, long time_checks) {
        SPRSupport.save(time_check, time_checks, context);
    }

    public static long getTimeCheck(Context context) {
        return SPRSupport.getLong(time_check, context);
    }

    public static boolean isValidateURl(String msg) {
        return !msg.startsWith("http://") && !msg.startsWith("https://");
    }

    public static void saveCatID(Context context, String type) {
        SPRSupport.save(TYPE_CAT_ID, type, context);
    }

    public static String getCatId(Context context) {
        return SPRSupport.getString(TYPE_CAT_ID, context, TYPE_CAT_ID);
    }

    public static void saveCountLike(String id, Context context, int countlike) {
        SPRSupport.save("count_like" + id, countlike, context);
    }

    public static int getCountLike(String id, Context context) {
        return SPRSupport.getInt("count_like" + id, context, 0);
    }

    public static void saveIsLike(String id, Context context, boolean islike) {
        SPRSupport.save("like" + id, islike, context);
    }

    public static boolean isLike(String id, Context context) {
        return SPRSupport.getBool("like" + id, context, false);
    }

    public static void saveIdOnesignal(Context context, String id_onesignals) {
        SPRSupport.save(id_onesignal, id_onesignals, context);
    }

    public static String getIdOnesignal(Context context) {
        return SPRSupport.getString(id_onesignal, context, "");
    }

    public static void saveLinkOnesignal(Context context, String link_onesignals) {
        SPRSupport.save(link_onesignal, link_onesignals, context);
    }

    public static String getLinkOnesignal(Context context) {
        return SPRSupport.getString(link_onesignal, context, "");
    }

    public static void saveLinkShow(Context context, String link_show_content) {
        SPRSupport.save(link_show, link_show_content, context);
    }

    public static String getLinkShow(Context context) {
        return SPRSupport.getString(link_show, context, "");
    }

    public static final String lastMessageId = "lastMessageId";
    public static final String time_last = "time_last";
    public static final String status_request_last = "status_request_last";
    public static final String id_token = "id_token";

    public static void saveToken(Context context, String id_tokens) {
        SPRSupport.save(id_token, id_tokens, context);
    }

    public static String getToken(Context context) {
        return SPRSupport.getString(id_token, context, "");
    }

    public static void saveMsgId(Context context, String msgid) {
        SPRSupport.save(lastMessageId, msgid, context);
    }

    public static String getMsgId(Context context) {
        return SPRSupport.getString(lastMessageId, context, "0");
    }

    public static void saveTimeRequestLast(Context context) {
        SPRSupport.save(time_last, Calendar.getInstance().getTimeInMillis(), context);
    }

    public static long getTimeRequestLast(Context context) {
        return SPRSupport.getLong(time_last, context, Calendar.getInstance().getTimeInMillis());
    }

    public static void saveStatusRequestLast(Context context, String statusrequestlast) {
        SPRSupport.save(status_request_last, statusrequestlast, context);
    }

    public static String getStatusRequestLast(Context context) {
        return SPRSupport.getString(status_request_last, context, "new");
    }

    public static String getImei(Context context) {

        String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        try {
            AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
            androidId = adInfo.getId();

        } catch (Exception e) {
            Log.d("SonLv", "getImeiException: " + e.getMessage());
        }
        Log.d("SonLv", "getImei: " + androidId);
        return androidId;
    }
}
