package com.seedsacpe.acpeseeds.Net;

import android.content.Context;
import android.util.Log;

import com.telpoo.frame.model.BaseModel;
import com.telpoo.frame.model.TaskParams;
import com.telpoo.frame.net.BaseNetSupport;
import com.telpoo.frame.object.BaseObject;

/**
 * Created by naq on 05/04/2015.
 */
public class TaskNet extends MyTask {
    NetData dataApi;

    public TaskNet() {
        super();
    }

    public TaskNet(BaseModel model, int taskType, Context context) {
        super(model, taskType, context);
    }

    public TaskNet(BaseModel model, Context context) {
        super(model, context);
    }

    @Override
    protected Boolean doInBackground(TaskParams... params) {
        if (context == null) {
            return TASK_FAILED;
        }
        if (context != null && !BaseNetSupport.isNetworkAvailable(context)) {
            msg = "Không có kết nối internet";
            return TASK_FAILED;

        }

        switch (taskType) {
            case TASK_CATEGORIES:
                dataApi = NetSupport.getCategories();
                break;

            case TASK_ITEMS:
                dataApi = NetSupport.getItems(getParramObject());
                break;

            case TASK_ADS:
                dataApi = NetSupport.getAds(context);
                break;
            case TASK_UPDATELIKE:
                updateLike();
                break;
            case TASK_DOWNLOAD:
                pushViewDownoad();
                break;
            case TASK_VIEW:
                pushViewDownoad();
                break;
            case TASK_GET_COMMENT:
                dataApi = NetSupport.getComment(getParramObject());
                break;
            case TASK_POST_COMMENT:
                postComment();
                break;
            case TASK_SEARCH:
                search();
                break;
            case TASK_SMALL_LIST:
                smallList();
                break;
            case TASK_REQUEST_V2:
                pushRequestV2();
                break;

        }
        return processData();
    }

    public Boolean pushRequestV2() {
        Log.d("telpoo", "getParramObject: " + getParramObject().toJson());
        dataApi = NetSupport.getRequestDataV2(context);
        if (dataApi.getcode() != 1) {
            msg = dataApi.getMsg();
            return TASK_FAILED;
        }
        setDataReturn(dataApi.getData());
        return TASK_DONE;
    }


    public boolean processData() {
        if (dataApi == null) return TASK_FAILED;
        if (dataApi.getcode() != 1) {
            msg = dataApi.getMsg();
            return TASK_FAILED;
        }
        setDataReturn(dataApi.getData());
        return TASK_DONE;
    }

    public Boolean updateLike() {
        Log.d("telpoo", "getParramObject: " + getParramObject().toJson());
        dataApi = NetSupport.postLike(getParramObject());
        if (dataApi.getcode() != 1) {
            msg = dataApi.getMsg();
            return TASK_FAILED;
        }
        setDataReturn(dataApi.getData());
        return TASK_DONE;
    }

    public Boolean pushViewDownoad() {
        Log.d("telpoo", "getParramObject: " + getParramObject().toJson());
        dataApi = NetSupport.postViewDownload(getParramObject());
        if (dataApi.getcode() != 1) {
            msg = dataApi.getMsg();
            return TASK_FAILED;
        }
        setDataReturn(dataApi.getData());
        return TASK_DONE;
    }

    public Boolean postComment() {
        Log.d("telpoo", "getParramObject: " + getParramObject().toJson());
        dataApi = NetSupport.postComment(getParramObject());
        if (dataApi.getcode() != 1) {
            msg = dataApi.getMsg();
            return TASK_FAILED;
        }
        setDataReturn(dataApi.getData());
        return TASK_DONE;
    }

    private Boolean search() {
        dataApi = NetSupport.searchName(getParramObject());
        if (dataApi.getcode() != 1) {
            msg = dataApi.getMsg();
            return TASK_FAILED;
        }
        setDataReturn(dataApi.getData());
        return TASK_DONE;
    }

    private Boolean smallList() {
        dataApi = NetSupport.smallList(getParramObject());
        if (dataApi.getcode() != 1) {
            msg = dataApi.getMsg();
            return TASK_FAILED;
        }
        setDataReturn(dataApi.getData());
        return TASK_DONE;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

    }


    public TaskNet clone() {
        TaskNet task = new TaskNet();
        task.setAllData(this.getAllData());
        return task;
    }


    public BaseObject getParramObject() {
        BaseObject object = getTaskParramBaseObject("parram");
        if (object == null) object = new BaseObject();
        return object;
    }
}
