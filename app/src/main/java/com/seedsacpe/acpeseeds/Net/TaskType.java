package com.seedsacpe.acpeseeds.Net;

/**
 * Created by naq on 05/04/2015.
 */
public interface TaskType {
    public static final int TASK_CATEGORIES = 0;
    public static final int TASK_ITEMS = 1;
    public static final int TASK_ADS = 2;
    public static final int TASK_UPDATELIKE = 3;
    public static final int TASK_DOWNLOAD = 4;
    public static final int TASK_VIEW = 5;
    public static final int TASK_GET_COMMENT = 6;
    public static final int TASK_POST_COMMENT = 7;
    public static final int TASK_SEARCH = 8;
    public static final int TASK_SMALL_LIST = 9;

    public static final int TASK_PUSH_COUNT = 10;
    public static final int TASK_REQUEST = 11;
    public static final int TASK_ONESIGNAL = 12;
    public static final int TASK_REQUEST_V2 = 14;
}
