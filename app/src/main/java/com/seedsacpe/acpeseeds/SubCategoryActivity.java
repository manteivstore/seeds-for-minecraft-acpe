package com.seedsacpe.acpeseeds;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.seedsacpe.acpeseeds.Net.TaskNet;
import com.seedsacpe.acpeseeds.action.DialogsSupport;
import com.seedsacpe.acpeseeds.action.Listener;
import com.seedsacpe.acpeseeds.adapter.ItemsAdapterHolder;
import com.seedsacpe.acpeseeds.adapter.ViewPagerAdapter;
import com.seedsacpe.acpeseeds.ads.AdsLoader;
import com.seedsacpe.acpeseeds.bean.CategoryObj;
import com.google.android.material.tabs.TabLayout;
import com.seedsacpe.acpeseeds.updatecontent.SettingSuppost;
import com.telpoo.frame.model.BaseModel;
import com.telpoo.frame.net.BaseNetSupport;
import com.telpoo.frame.object.BaseObject;
import com.telpoo.frame.utils.JsonSupport;
import com.telpoo.frame.utils.KeyboardSupport;

import org.json.JSONException;

import java.util.ArrayList;

public class SubCategoryActivity extends SwipeBackActivity implements View.OnClickListener {
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    TabLayout tabLayout;
    TextView tvType;
    TextView tvVersion;
    BaseObject object = new BaseObject();

    LinearLayout lnViewType;
    LinearLayout lnViewTextSearch;
    RelativeLayout rlBtSearch;
    ImageView imgBackSearch;
    ImageView imgCancelSearch;
    EditText edtSearch;
    RecyclerView rcListSearch;

    boolean isCheckSearch = false;
    String s = "";
    BaseObject objectRequest = new BaseObject();
    TaskNet taskNet;
    ItemsAdapterHolder searchAdapterHolder;
    ArrayList<BaseObject> list = new ArrayList<>();
    ArrayList<BaseObject> datas;

    BaseModel baseModel = new BaseModel() {
        @Override
        public void onSuccess(int taskType, Object data, String msg) {
            super.onSuccess(taskType, data, msg);
            closeProgressDialog();
            switch (taskType) {
                case TASK_CATEGORIES:
                    datas = (ArrayList<BaseObject>) data;
                    for (int i = 0; i < datas.size(); i++) {
                        object = datas.get(i);
                        String type = object.get(CategoryObj.id, "");
                        Log.d("DucQv", "type: " + type);
                        if (type.equals("seeds")) {
                            initData();
                            return;
                        }
                    }
                    break;

                case TASK_SEARCH:
                    list = (ArrayList<BaseObject>) data;
                    searchAdapterHolder.setData(list);
                    break;
            }
        }

        @Override
        public void onFail(int taskType, String msg) {
            super.onFail(taskType, msg);
            closeProgressDialog();
            switch (taskType) {
                case TASK_CATEGORIES:
                    Toast.makeText(SubCategoryActivity.this, msg, Toast.LENGTH_LONG).show();
                    if (!BaseNetSupport.isNetworkAvailable(SubCategoryActivity.this))
                        DialogsSupport.showDialogYes(SubCategoryActivity.this, "Clode", "Network not connected!", null, new Listener.OnDialogYesNoListener() {
                            @Override
                            public void onYesClick() {
                                finish();
                            }

                            @Override
                            public void onNoClick() {

                            }
                        });
                    break;

                case TASK_SEARCH:

                    break;

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        initView();
        showProgressDialog(this);
        loadCategories();
        AdsLoader.getInstall(this).loadAdsFull();
        AdsLoader.getInstall(this).loadAdsVideos();
    }

    public String getCategoryName() {
        return object.get(CategoryObj.name, "");
    }

    private void initView() {
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.navigation);
        tvType = findViewById(R.id.tvType);
        tvVersion = findViewById(R.id.tvVersion);
        lnViewType = findViewById(R.id.lnViewType);
        lnViewTextSearch = findViewById(R.id.lnViewTextSearch);
        rlBtSearch = findViewById(R.id.rlBtSearch);
        imgBackSearch = findViewById(R.id.imgBackSearch);
        imgCancelSearch = findViewById(R.id.imgCancelSearch);
        edtSearch = findViewById(R.id.edtSearch);
        rcListSearch = findViewById(R.id.rcListSearch);
        lnViewType.setVisibility(View.VISIBLE);
        rlBtSearch.setVisibility(View.VISIBLE);
        lnViewTextSearch.setVisibility(View.GONE);
        imgCancelSearch.setVisibility(View.GONE);
    }

    private void initData() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        Log.d("DucQv", "initData_object: " + object.toJson());
        tvType.setText(getResources().getText(R.string.app_name));
        tvVersion.setText(BuildConfig.VERSION_NAME);
        try {
            adapter.setData(JsonSupport.jsonArray2BaseOj(object.get(CategoryObj.menus, "")));
            tabLayout.setupWithViewPager(viewPager);
        } catch (JSONException e) {
            MyApplication.getInstance().trackException(e);
        }

        rlBtSearch.setOnClickListener(this);
        imgBackSearch.setOnClickListener(this);
        imgCancelSearch.setOnClickListener(this);

        searchAdapterHolder = new ItemsAdapterHolder(this);
        rcListSearch.setHasFixedSize(true);
        rcListSearch.setItemAnimator(new DefaultItemAnimator());
        rcListSearch.setLayoutManager(new LinearLayoutManager(this));
        rcListSearch.setAdapter(searchAdapterHolder);
        rcListSearch.setOnTouchListener((view1, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_UP:
                case MotionEvent.ACTION_POINTER_DOWN:
                case MotionEvent.ACTION_MOVE:
                    KeyboardSupport.hideKeyboard(this, edtSearch);
                    break;
            }
            return false;
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                s = editable.toString();
                if (!s.isEmpty()) {
                    objectRequest = new BaseObject();
                    objectRequest.set("s", s);
                    imgCancelSearch.setVisibility(View.VISIBLE);
                    rcListSearch.setVisibility(View.VISIBLE);
                } else {
                    imgCancelSearch.setVisibility(View.GONE);
                    rcListSearch.setVisibility(View.GONE);
                }
                loadSearch();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        tvVersion.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBtSearch:
                isCheckSearch(isCheckSearch);
                isCheckSearch = !isCheckSearch;
                break;
            case R.id.imgBackSearch:
                isCheckSearch(isCheckSearch);
                KeyboardSupport.hideKeyboard(this, edtSearch);
                isCheckSearch = !isCheckSearch;
                break;
            case R.id.imgCancelSearch:
                edtSearch.setText("");
                break;
        }
    }

    private void isCheckSearch(boolean isCheck) {
        if (!isCheck) {
            lnViewType.setVisibility(View.GONE);
            rlBtSearch.setVisibility(View.GONE);
            lnViewTextSearch.setVisibility(View.VISIBLE);
            edtSearch.requestFocus();
            KeyboardSupport.showKeyboard(this, edtSearch);
        } else {
            lnViewType.setVisibility(View.VISIBLE);
            rlBtSearch.setVisibility(View.VISIBLE);
            lnViewTextSearch.setVisibility(View.GONE);
            edtSearch.setText("");
        }
    }

    private void loadCategories() {
        taskNet = new TaskNet(baseModel, TASK_CATEGORIES, this);
        taskNet.exe();
    }

    public void loadSearch() {
        taskNet = new TaskNet(baseModel, TASK_SEARCH, this);
        taskNet.setTaskParram("parram", objectRequest);
        taskNet.exe();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("SonLv", "onDestroy: ");
        if (!SettingSuppost.isCheckSub(this)) return;
        if (!SettingSuppost.isCheckProduct(this)) return;
        SettingSuppost.isHideShowApp(this);
    }

    @Override
    public void onBackPressed() {
        if (isCheckSearch) return;
        super.onBackPressed();

    }
}
