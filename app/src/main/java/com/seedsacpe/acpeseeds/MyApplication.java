package com.seedsacpe.acpeseeds;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;

import com.seedsacpe.acpeseeds.Net.SPRsupport;
import com.seedsacpe.acpeseeds.ads.AdsLoader;
import com.seedsacpe.acpeseeds.ads.AdsSuppost;
import com.seedsacpe.acpeseeds.updatecontent.UpdateWorker;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.Constants;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.seedsacpe.acpeseeds.action.Listener;
import com.telpoo.frame.utils.SPRSupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MyApplication extends Application {
    private static MyApplication mInstance;
    Listener.OnPurchasesListener onPurchasesListener;

    BillingProcessor bp;

    @Override
    public void onCreate() {
        super.onCreate();
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getApplicationContext());
        config.defaultDisplayImageOptions(getImageOption());
        config.writeDebugLogs(); // Remove for release app
        ImageLoader.getInstance().init(config.build());
        initFCM();
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        AdsLoader.getInstall(this).initSDK(this);
        AdsLoader.getInstall(this).initAds();
        if (SPRsupport.getintall(this) == 0) {
            Log.d("Ducqv", "saveTimeIntallContent: ");
            AdsSuppost.getInstall(this).saveTimeIntallContent();
            SPRsupport.saveintall(this, SPRsupport.getintall(this) + 1);
        }

        UpdateWorker.start(this);

        mInstance = this;

        if (!SPRSupport.contain("app_install", this)) {
            SPRSupport.save("app_install", "ok", this);
            trackEvent("install", "install", "install");
        }

        bp = BillingProcessor.newBillingProcessor(this, getString(R.string.apikey)
                , getString(R.string.id_app), new BillingProcessor.IBillingHandler() {
                    @Override
                    public void onBillingInitialized() {
                    }

                    @Override
                    public void onProductPurchased(String productId, TransactionDetails details) {
                        Log.d("billingClient", "productId: " + productId);
                        Log.d("billingClient", "responseData: " + details.purchaseInfo.responseData);
                        Log.d("billingClient", "purchaseData: " + details.purchaseInfo.purchaseData);

                        MyApplication.getInstance().trackEvent("Billing", "OK", "Success");
                        if (onPurchasesListener != null) onPurchasesListener.onSuccess();
                    }

                    @Override
                    public void onBillingError(int errorCode, Throwable error) {
                        Log.d("billingClient", "onBillingError: " + errorCode);
                        String sms = "";
                        if (error != null) sms = error.getMessage();

                        switch (errorCode) {
                            case Constants.BILLING_ERROR_FAILED_LOAD_PURCHASES:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_FAILED_LOAD_PURCHASES", "Error in loadPurchasesByType " + sms);
                                break;

                            case Constants.BILLING_ERROR_FAILED_TO_INITIALIZE_PURCHASE:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_FAILED_TO_INITIALIZE_PURCHASE", "BILLING_ERROR_FAILED_TO_INITIALIZE_PURCHASE");
                                break;

                            case Constants.BILLING_ERROR_INVALID_SIGNATURE:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_INVALID_SIGNATURE", "Public key signature doesn't match! " + sms);
                                break;
                            case Constants.BILLING_ERROR_LOST_CONTEXT:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_LOST_CONTEXT", "BILLING_ERROR_LOST_CONTEXT");
                                break;

                            case Constants.BILLING_ERROR_INVALID_MERCHANT_ID:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_INVALID_MERCHANT_ID", "Invalid or tampered merchant id! " + sms);
                                break;

                            case Constants.BILLING_ERROR_OTHER_ERROR:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_OTHER_ERROR", "Error in purchase " + sms);
                                break;

                            case Constants.BILLING_ERROR_CONSUME_FAILED:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_CONSUME_FAILED", "Error in consumePurchase " + sms);
                                break;

                            case Constants.BILLING_ERROR_SKUDETAILS_FAILED:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_SKUDETAILS_FAILED", "Failed to call getSkuDetails " + sms);
                                break;

                            case Constants.BILLING_ERROR_BIND_PLAY_STORE_FAILED:
                                MyApplication.getInstance().trackEvent("Billing", "BILLING_ERROR_BIND_PLAY_STORE_FAILED", "error in bindPlayServices " + sms);
                                break;
                        }
                    }

                    @Override
                    public void onPurchaseHistoryRestored() {

                        Log.d("billingClient", "onPurchaseHistoryRestored");
                    }
                });
        bp.initialize();
    }

    public void initFCM() {
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("SonLv", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        // Log and toast
                        Log.d("SonLv", "token: " + task.getResult().getToken());
                        SPRsupport.saveToken(getApplicationContext(), task.getResult().getToken());

                    }
                });
    }

    public boolean isInitialized() {
        if (bp != null && bp.isInitialized()) {
            Log.d("billingClient", "Đã Initialized");
            return true;
        }

        bp.initialize();
        return false;
    }


    public boolean isSubSuccess() {
        if (!AdsSuppost.getInstall(this).isIAP()) return false;
        if (!isInitialized()) return false;

        //load data Purchases từ google về. Nếu không thì sẽ bị cache
        bp.loadOwnedPurchasesFromGoogle();
        try {
            JSONArray list = new JSONArray(AdsSuppost.getInstall(this).getDataAds().optString("list_sub", ""));

            for (int i = 0; i < list.length(); i++) {
                JSONObject item = list.getJSONObject(i);
                String id = item.optString("id", "");
                if (item.optBoolean("disable", true)) continue;
                try {
                    TransactionDetails details = bp.getSubscriptionTransactionDetails(AdsSuppost.getInstall(getApplicationContext()).getSubId());
                    Log.d("billingClient", "purchaseTime: " + details.purchaseInfo.purchaseData.purchaseTime);
                    Log.d("billingClient", "purchaseState: " + details.purchaseInfo.purchaseData.purchaseState);
                    Log.d("billingClient", "autoRenewing: " + details.purchaseInfo.purchaseData.autoRenewing);
                    Log.d("billingClient", id + " isSubscribed: " + bp.isSubscribed(id));

                    if (!details.purchaseInfo.purchaseData.autoRenewing) {
                        Log.d("billingClient", "Đã hủy sub");
                        return false;
                    }

                } catch (Exception e) {

                }

                if (bp.isSubscribed(id)) return true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isPurchaseSuccess() {
        if (!AdsSuppost.getInstall(this).isProduct()) return false;
        if (!isInitialized()) return false;

//load data Purchases từ google về. Nếu không thì sẽ bị cache
        bp.loadOwnedPurchasesFromGoogle();
        try {
            JSONArray list = new JSONArray(AdsSuppost.getInstall(this).getDataAds().optString("list_product", ""));

            for (int i = 0; i < list.length(); i++) {
                JSONObject item = list.getJSONObject(i);
                String id = item.optString("id", "");
                if (item.optBoolean("disable", true)) continue;
                try {
                    TransactionDetails details = bp.getSubscriptionTransactionDetails(AdsSuppost.getInstall(getApplicationContext()).getProductId());
                    Log.d("billingClient", "purchaseTime: " + details.purchaseInfo.purchaseData.purchaseTime);
                    Log.d("billingClient", "purchaseState: " + details.purchaseInfo.purchaseData.purchaseState);
                    Log.d("billingClient", "autoRenewing: " + details.purchaseInfo.purchaseData.autoRenewing);
                    Log.d("billingClient", id + " isPurchased: " + bp.isPurchased(id));

                    if (!details.purchaseInfo.purchaseData.autoRenewing) {
                        Log.d("billingClient", "Đã hủy thanh toán");
                        return false;
                    }

                } catch (Exception e) {

                }

                if (bp.isPurchased(id)) return true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static DisplayImageOptions getImageOption() {
        return new DisplayImageOptions.Builder()
                .delayBeforeLoading(0)
                .cacheOnDisk(true)
                .cacheInMemory(true) // default
                .cacheOnDisk(true) // default
                .considerExifParams(false) // default
                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                .displayer(new FadeInBitmapDisplayer(500, true, false, false))
                .build();
    }

    public void processIAP(Activity activity, Listener.OnPurchasesListener onPurchasesListener) {
        this.onPurchasesListener = onPurchasesListener;
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Log.d("billingClient", "isIabServiceAvailable: false");
            onPurchasesListener.onError();
            return;
        }

        if (!isInitialized()) return;

        if (!bp.isOneTimePurchaseSupported()) {
            Log.d("billingClient", "isOneTimePurchaseSupported: false");
            onPurchasesListener.onError();
            return;
        }

        if (!bp.isSubscriptionUpdateSupported()) {
            Log.d("billingClient", "isSubscriptionUpdateSupported: false");
            onPurchasesListener.onError();
            return;
        }
        String id = AdsSuppost.getInstall(this).getSubId();
        Log.d("billingClient", "call subscribe->" + id);
        bp.subscribe(activity, id);

    }

    public void processProductIAP(Activity activity, Listener.OnPurchasesListener onPurchasesListener) {
        this.onPurchasesListener = onPurchasesListener;
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Log.d("billingClient", "isIabServiceAvailable: false");
            onPurchasesListener.onError();
            return;
        }

        if (!isInitialized()) return;

        if (!bp.isOneTimePurchaseSupported()) {
            Log.d("billingClient", "isOneTimePurchaseSupported: false");
            onPurchasesListener.onError();
            return;
        }

        if (!bp.isSubscriptionUpdateSupported()) {
            Log.d("billingClient", "isSubscriptionUpdateSupported: false");
            onPurchasesListener.onError();
            return;
        }
        String id = AdsSuppost.getInstall(this).getProductId();
        Log.d("billingClient", "call subscribe product->" + id);
        bp.purchase(activity, id);
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Log.d("Tracker", "screenName: " + screenName);
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Log.d("Tracker", "trackException: " + e.getMessage());
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    public void trackEvent(String category, String action, String label) {
        Log.d("Tracker", "trackEvent: " + category + " | " + action + " | " + label);
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }
}
