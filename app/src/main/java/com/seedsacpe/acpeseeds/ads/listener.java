package com.seedsacpe.acpeseeds.ads;

public class listener {

    public interface adsLoadShowFull {
        void onLoadFailAds();

        void onAdClosed();
    }

    public interface adsLoadShowVideos {
        void onLoadFailAds();

        void onAdClosed();
    }
}
