package com.seedsacpe.acpeseeds.ads;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.telpoo.frame.utils.SPRSupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class AdsSuppost {
    public static String time_show_content = "time_show_content";
    public static String timeShowAds = "timeShowAds";

    public static AdsSuppost settingAdsSuppost;
    Context context;
    private String TAG = "AdmobLoader";

    public static AdsSuppost getInstall(Context context) {
        if (settingAdsSuppost == null) settingAdsSuppost = new AdsSuppost(context);
        return settingAdsSuppost;
    }

    public AdsSuppost(Context context) {
        this.context = context;
    }

    public int getTimeShow() {
        return getDataAds().optInt("time_show", 1) * 1000 * 60;
    }

    public void saveTimeShowAds() {
        SPRSupport.save(timeShowAds, Calendar.getInstance().getTimeInMillis(), context);
    }

    public static long getTimeShowAds(Context context) {
        return SPRSupport.getLong(timeShowAds, context,Calendar.getInstance().getTimeInMillis());
    }

    public void saveTimeIntallContent() {
        SPRSupport.save(time_show_content, Calendar.getInstance().getTimeInMillis(), context);
    }

    public static long getTimeIntallContent(Context context) {
        return SPRSupport.getLong(time_show_content, context,Calendar.getInstance().getTimeInMillis());
    }

    public int getTimeShowContent() {
        return getDataAds().optInt(time_show_content, 1) * 1000 * 60;
    }

    public boolean enableShowContent() {
        long current = Calendar.getInstance().getTimeInMillis();
        long last = getTimeIntallContent(context);
        Log.d("SonLv", "timeShowContent: " + (current - last) + "/" + getTimeShowContent());
        return (current - last > getTimeShowContent());
    }

    public boolean enableShowAds() {
        if (!SPRSupport.contain(timeShowAds, context)) {
            return true;
        }
        long current = Calendar.getInstance().getTimeInMillis();
        long last = getTimeShowAds(context);
        Log.d(TAG, "checkshow: " + (current - last) + "/" + getTimeShow());
        return (current - last > getTimeShow());
    }

    public AdRequest getAdRequest() {
        return new AdRequest.Builder()
                .addTestDevice("DF2084CA2BC8DCAEBA3F3382D255853D")
                .build();
    }

    public static void saveDataAds(JSONObject data, Context context) {
        SPRSupport.save("dataAds", data.toString(), context);
    }

    public JSONObject getDataAds() {
        JSONObject ads = new JSONObject();
        try {
            ads = new JSONObject(SPRSupport.getString("dataAds", context));
        } catch (JSONException e) {
            Log.d(TAG, "JSONException: " + e.getMessage());
            e.printStackTrace();
        }
        return ads;
    }

    public boolean isAdsBlock() {
        try {
            return getDataAds().getJSONObject("ads_block").optBoolean("is_block", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getImageBanner() {
        try {
            return getDataAds().getJSONObject("ads_block").optString("image_banner", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getImageFull() {
        try {
            return getDataAds().getJSONObject("ads_block").optString("image_full", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getLinkAds() {
        try {
            return getDataAds().getJSONObject("ads_block").optString("link", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getPackageNameAds() {
        try {
            return getDataAds().getJSONObject("ads_block").optString("package", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean isDebug() {
        return getDataAds().optBoolean("debug", false);
    }

    public String getSubId() {
        try {
            JSONArray list = new JSONArray(getDataAds().optString("list_sub", ""));
            for (int i = 0; i < list.length(); i++) {
                JSONObject item = list.getJSONObject(i);
                if (item.optBoolean("disable", true)) continue;
                return item.optString("id", "");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "month_5dola";
    }

    public String getProductId() {
        try {
            JSONArray list = new JSONArray(getDataAds().optString("list_product", ""));
            for (int i = 0; i < list.length(); i++) {
                JSONObject item = list.getJSONObject(i);
                if (item.optBoolean("disable", true)) continue;
                return item.optString("id", "");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "full_product";
    }


    public boolean isIAP() {
        return getDataAds().optBoolean("iap", false);
    }
    public boolean isProduct() {
        return getDataAds().optBoolean("is_product", false);
    }

    public boolean isAds() {
        return getDataAds().optBoolean("is_ads", false);
    }

    public boolean isLoadAds() {
        return getDataAds().optBoolean("is_loadads", false);
    }

    public boolean isAPK() {
        return getDataAds().optBoolean("is_apk", false);
    }

    public boolean isInHide() {
        return getDataAds().optBoolean("is_in_hide", false);
    }

    public boolean isOutHide() {
        return getDataAds().optBoolean("is_out_hide", false);
    }

    public boolean isTimeContent() {
        return getDataAds().optBoolean("is_time_content", false);
    }

    public boolean isCheckCode() {
        if (getUrlCheckCode().isEmpty()) return false;
        try {
            return getDataAds().getJSONObject("get_code").optBoolean("enable", false);
        } catch (Exception e) {
            return false;
        }
    }

    public String getUrlCheckCode() {
        try {
            return getDataAds().getJSONObject("get_code").optString("link", "");
        } catch (Exception e) {
            return "";
        }
    }
}
