package com.seedsacpe.acpeseeds.ads;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.ads.LoadAdError;
import com.seedsacpe.acpeseeds.Net.SPRsupport;
import com.seedsacpe.acpeseeds.Net.TaskNet;
import com.seedsacpe.acpeseeds.Net.TaskType;
import com.seedsacpe.acpeseeds.R;
import com.seedsacpe.acpeseeds.onesignal.EventFirebase;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.telpoo.frame.model.BaseModel;

public class AdsLoader {
    Context context;
    public static AdsLoader adsLoader;
    private RewardedVideoAd mRewardedVideoAd;
    private PublisherInterstitialAd mPublisherInterstitialAd;

    boolean isCheckRewardedVideoAd;

    public static AdsLoader getInstall(Context context) {
        if (adsLoader == null) adsLoader = new AdsLoader(context);
        return adsLoader;
    }

    public AdsLoader(Context context) {
        this.context = context;
    }

    public void initSDK(Context context) {
        MobileAds.initialize(context);
    }

    public void initAds() {
        TaskNet net = new TaskNet(new BaseModel() {
            @Override
            public void onSuccess(int taskType, Object data, String msg) {
                super.onSuccess(taskType, data, msg);
                Log.d("DucQv", "onSuccess: ");
            }

            @Override
            public void onFail(int taskType, String msg) {
                super.onFail(taskType, msg);
                Log.d("DucQv", "onFail: " + msg);
            }
        }, TaskType.TASK_ADS, context);
        net.exe();
    }

    private PublisherAdRequest getPublisherAdRequest() {
        return new PublisherAdRequest.Builder().addTestDevice("24989C51973F93EEC5BA8A5DA737CFB5").build();
    }

    public void showBanner(ViewGroup viewGroup, AdSize adSize, int index) {
        if (!AdsSuppost.getInstall(context).isAds()) return;
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.activity_ads, null);
        LinearLayout lnShowBanner = view.findViewById(R.id.lnBannerView);
        viewGroup.addView(view);

        final String id = SPRsupport.getIdBannerAdmobRandomList(context, index);
        Log.d("DucQv", "Id_banner: " + id);
        if (id.isEmpty()) {
            Log.d("DucQv", "Id banner empty");
            return;
        }
        PublisherAdView adView = new PublisherAdView(context);
        adView.setAdSizes(adSize);
        adView.setAdUnitId(id);
        adView.loadAd(getPublisherAdRequest());
        lnShowBanner.addView(adView);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                viewGroup.setVisibility(View.VISIBLE);
                Log.d("DucQv", "onAdLoaded");
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                viewGroup.setVisibility(View.GONE);
                Log.d("DucQv", "onAdFailedToLoad");
            }
        });
    }

    public void loadAdsFull() {
        if (!AdsSuppost.getInstall(context).isAds()) {
            return;
        }
        String country = context.getResources().getConfiguration().locale.getCountry();
        Log.d("DucQv", "country: " + country);
        String listUser[] = SPRsupport.getCountrySub(context).split("#");
        for (int i = 0; i < listUser.length; i++) {
            if (country.equals(listUser[i])) {
                Log.d("DucQv", "Dừng quốc gia: " + country);
                return;
            }
        }
        String id_full = SPRsupport.getIdFullAdmob(context);
        Log.d("DucQv", "id_full: " + id_full);
        if (id_full.isEmpty()) {
            Log.d("DucQv", "id_isEmpty: ");
            return;
        }
        mPublisherInterstitialAd = new PublisherInterstitialAd(context);
        mPublisherInterstitialAd.setAdUnitId(id_full);
        mPublisherInterstitialAd.loadAd(getPublisherAdRequest());
    }

    public void showFullAdmob(final listener.adsLoadShowFull adsLoadShowFull) {
        if (!AdsSuppost.getInstall(context).enableShowAds()) {
            adsLoadShowFull.onLoadFailAds();
            Log.d("DucQv", "chưa đến giờ show full: ");
            return;
        }
        if (mPublisherInterstitialAd == null) {
            adsLoadShowFull.onLoadFailAds();
            loadAdsFull();
            return;
        }
        if (mPublisherInterstitialAd.isLoaded()) {
            mPublisherInterstitialAd.show();

            mPublisherInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    adsLoadShowFull.onAdClosed();
                    AdsSuppost.getInstall(context).saveTimeShowAds();
                    loadAdsFull();
                }
            });
            return;
        }
        adsLoadShowFull.onLoadFailAds();
    }

    public void loadAdsVideos() {
        String idVideo = SPRsupport.getIdVideoAdmob(context);
        Log.d("DucQv", "id_VideoAdsmob: " + idVideo);
        if (idVideo.isEmpty()) {
            Log.d("DucQv", "idVideos_isEmpty: " + idVideo);
            return;
        }
        String country = context.getResources().getConfiguration().locale.getCountry();
        Log.d("DucQv", "country: " + country);
        String listUser[] = SPRsupport.getCountrySub(context).split("#");
        for (int i = 0; i < listUser.length; i++) {
            if (country.equals(listUser[i])) {
                Log.d("DucQv", "Dừng quốc gia: " + country);
                return;
            }
        }
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
        mRewardedVideoAd.loadAd(idVideo, new AdRequest.Builder().build());
    }

    public void showVideoAdmob(listener.adsLoadShowVideos adsLoadShowVideos) {
        if (mRewardedVideoAd == null) {
            adsLoadShowVideos.onLoadFailAds();
            loadAdsVideos();
            return;
        }
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
            mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
                @Override
                public void onRewardedVideoAdLoaded() {
                    EventFirebase.getInstall(context).logEvent("show_video");
                    Log.d("DucQv", "onRewardedVideoAdLoaded");
                }

                @Override
                public void onRewardedVideoAdOpened() {

                }

                @Override
                public void onRewardedVideoStarted() {
                    isCheckRewardedVideoAd = false;
                }

                @Override
                public void onRewardedVideoAdClosed() {
                    loadAdsVideos();
                    if (!isCheckRewardedVideoAd) {
                        Log.d("DucQv", "Không xem hết video");
                        return;
                    }
                    adsLoadShowVideos.onAdClosed();
                    EventFirebase.getInstall(context).logEvent("close_video");
                }

                @Override
                public void onRewarded(RewardItem rewardItem) {
                    isCheckRewardedVideoAd = true;

                }

                @Override
                public void onRewardedVideoAdLeftApplication() {

                }

                @Override
                public void onRewardedVideoAdFailedToLoad(int i) {
                    adsLoadShowVideos.onLoadFailAds();
                    EventFirebase.getInstall(context).logEvent("false_video");
                    Log.d("DucQv", "onRewardedVideoAdFailedToLoad_video");
                }

                @Override
                public void onRewardedVideoCompleted() {
                    Log.d("DucQv", "onRewardedVideoCompleted: ");
                }
            });
            return;
        }
        adsLoadShowVideos.onLoadFailAds();

    }

    private static void showDialogAds(Context context, ViewGroup viewGroup) {
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.dialog_ads, null);
        viewGroup.setVisibility(View.VISIBLE);
        viewGroup.addView(view);
    }

    private static void closeDialogAds(ViewGroup viewGroup) {
        viewGroup.setVisibility(View.GONE);
    }
}
